<?php

return [

    'plugin' => [
        'name'        => 'Utilities',
        'description' => 'Utilities to make October CMS a bit easier to manage',
        'icon'        => 'icon-cog',
    ],

    'permissions' => [
        'variables' => 'Change theme variables',
        'forms'     => 'Amend Frontend Forms',
    ],

    'components' => [
        'forms' => [
            'name'         => 'Display Form',
            'description'  => 'Select a Form to display to the user',
            "which"        => "Select Form",
            'success'      => "Thank you. We'll be in touch as soon as possible.",
            "honeypot"     => "Thanks.",
            "notification" => "Form Notification",
            "autoresponse" => "Form Autoresponse",
        ]
    ],

    'variables' => [
        'name'        => 'Theme and Utilities Variables',
        'description' => 'Manage colours, fonts, and other variables',
        'icon'        => 'icon-cog',
        'fields' => [
            'font_size'   => 'Font Size',
            'font_family' => 'Font Family',
            'line_height' => 'Line Height',
            'background_colour' => 'Background Colour',
            'colour'            => 'Colour',
            'general_spacing'   => 'General Spacing',
            'inner_spacing'     => 'Inner Spacing',
            'primary_colour'    => 'Primary Colour',
            'secondary_colour'  => 'Secondary Colour',
        ],
        'tabs' => [
            'body' => 'Body',
            'header1' => 'Header 1',
            'header2' => 'Header 2',
            'header3' => 'Header 3',
            'header4' => 'Header 4',
            'general' => 'General',
        ],
        'messages' => [
            'updated' => 'Variables Updated',
            'save' => 'Update Theme Variables',
        ]
    ],

    'forms' => [
        'name'    => 'Forms',
        'icon'    => 'icon-wpforms',
        'edit'    => 'Create / Edit Form',
        'save'    => 'Save and Close',
        'cancel'  => 'Cancel',
        'return'  => 'Return to Forms',
        'saved'   => 'Form Saved',
        'deleted' => 'Form Deleted',
        'new'     => 'New Form',
        'delete'  => 'Delete Forms',
        'confirm_delete' => 'Are you sure you want to delete these forms?',
        'fields' => [
            'id' => 'ID',
            'name' => 'Form Name',
            'records' => 'Records',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'field_name' => 'Field Label',
            'placeholder' => 'Placeholder',
            'required' => 'Required',
            "slug" => "Slug",
            'text' => 'Text',
            'textarea' => 'Textarea',
            'general' => 'General Input',
            'selections' => 'Selections',
            'type' => 'Type',
            "checkbox" => "Checkboxes",
            "radio" => "Radio",
            "select" => "Dropdown",
            "email" => "Email",
            "number" => "Number",
            "date" => "Date",
            "telephone" => "Telephone",
            "url" => "URL",
            "color" => "Colour",
            "password" => "Password",
            "options" => "Options",
            "step" => "Step",
            "minlength" => "Minimum Length",
            "min" => "Minimum Value",
            "max" => "Maximum Value",
        ],
        'tabs' => [
            'notify' => 'Notify',
            'autoresponse' => 'Autoresponse',
            'form' => 'Form',
            'records' => 'Records',
        ]
    ],

    'csrf' => [
        'name' => 'Front-end CSRF',
        'label' => 'Exclude front-end routes from CSRF protection.',
        'description' => 'Exclude front-end routes from CSRF protection.',
        'prompt' => 'Add route exclusion',
        'route_label' => 'Route (api, https://example.tld/api, route/path/url, etc)'
    ],

];
