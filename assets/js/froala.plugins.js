(function($) {

    $.oc.richEditorButtons.push('columns');

    $.FroalaEditor.RegisterCommand('columns', {
        title: "Columns",
        type: 'dropdown',
        icon: '<i class="icon-columns"></i>',
        options: {
            'two-columns': '2 Columns',
            'three-columns': '3 Columns',
            'four-columns': '4 Columns',
            'two-columns-one-quarter-left': '2 Columns - 25% left-side',
            'two-columns-one-third-left': '2 Columns - 33% left-side',
            'two-columns-two-thirds-left': '2 Columns - 66% left-side',
            'two-columns-three-quarters-left': '2 Columns - 75% left-side',
            'three-columns-big-center': '3 Columns - 50% center',
        },

        undo: true,
        focus: true,
        refreshAfterCallback: true,

        callback: function(cmd, val) {
            var html = "";
            if (val == 'two-columns') {
                html = '<div class="grid two-columns"><div><p>Column 1</p></div><div><p>Column 2</p></div></div>';
            }
            if (val == 'three-columns') {
                html = '<div class="grid three-columns"><div><p>Column 1</p></div><div><p>Column 2</p></div><div><p>Column 3</p></div></div>';
            }
            if (val == 'four-columns') {
                html = '<div class="grid four-columns"><div><p>Column 1</p></div><div><p>Column 2</p></div><div><p>Column 3</p></div><div><p>Column 4</p></div></div>';
            }
            if (val == 'two-columns-one-quarter-left') {
                html = '<div class="grid two-columns one-quarter-left"><div><p>Column 1</p></div><div><p>Column 2</p></div></div>';
            }
            if (val == 'two-columns-one-third-left') {
                html = '<div class="grid two-columns one-third-left"><div><p>Column 1</p></div><div><p>Column 2</p></div></div>';
            }
            if (val == 'two-columns-two-thirds-left') {
                html = '<div class="grid two-columns two-thirds-left"><div><p>Column 1</p></div><div><p>Column 2</p></div></div>';
            }
            if (val == 'two-columns-three-quarters-left') {
                html = '<div class="grid two-columns three-quarters-left"><div><p>Column 1</p></div><div><p>Column 2</p></div></div>';
            }
            if (val == 'three-columns-big-center') {
                html = '<div class="grid three-columns big-centre"><div><p>Column 1</p></div><div><p>Column 2</p></div><div><p>Column 3</p></div></div>';
            }
            // insert an empty element afterwards so we can keep typing
            html += '<p>&nbsp;</p>';

            this.html.insert(html);
        }
    });
})(jQuery);