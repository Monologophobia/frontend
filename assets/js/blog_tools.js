document.addEventListener("DOMContentLoaded", function() {

    // If we're supplied a url like
    // /backend//rainlab/blog/posts/create?category_id=7
    // then automatically select categories
    var categories  = document.querySelectorAll('input[name="Post[categories][]"]');
    var urlParams = new URLSearchParams(window.location.search);
    var category_id = urlParams.get('category_id');
    if (categories && category_id) {
        for (var i = 0; i < categories.length; i++) {
            if (categories[i].value == category_id) {
                categories[i].checked = true;
                break;
            }
        }
    }

    // If we're supplied a url like
    // /backend//rainlab/blog/posts/create?context=create
    // automatically select published with correct date and time
    var context = urlParams.get('context');
    if (context == 'create') {
        // this isn't firing either so give it a quick delay
        setTimeout(function() {

            function pad(number) {
                if ( number < 10 ) {
                    return '0' + number;
                }
                return number;
            }

            document.querySelector('label[for="Form-field-Post-published"]').click();

            var now  = new Date();

            var date = document.querySelector('#DatePicker-formPublishedAt-date-published_at');
            var time = document.querySelector('#DatePicker-formPublishedAt-time-published_at');

            var day     = pad(now.getDate());
            var month   = pad(now.getMonth() + 1);
            var year    = now.getFullYear();
            var hours   = pad(now.getHours());
            var minutes = pad(now.getMinutes());

            date.value = day + "/" + month + "/" + year;
            time.value = hours + ":" + minutes;

            var published_at = document.querySelector('input[name="Post[published_at]"]');
            published_at.value = year + "-" + month + "-" + day + " " + hours + ":" + minutes;

        }, 300);
    }

});