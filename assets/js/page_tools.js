document.addEventListener("DOMContentLoaded", function() {
    // for some reason this isn't firing on window load
    // so add a small pause and then do it
    setTimeout(function() {
        var pages = document.querySelectorAll('#PageList-pageList-page-list li');
        const urlParams = new URLSearchParams(window.location.search);
        var page = urlParams.get('item_path');
        if (pages && page) {
            for (var i = 0; i < pages.length; i++) {
                if (pages[i].dataset.itemPath == page) {
                    pages[i].querySelector('a').click();
                    break;
                }
            }
        }
    }, 300);
});