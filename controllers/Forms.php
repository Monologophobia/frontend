<?php namespace Monologophobia\Utilities\Controllers;

use Flash;
use BackendAuth;
use BackendMenu;
use Backend\Classes\Controller;

use Monologophobia\Utilities\Models\Form;

class Forms extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['monologophobia.utilities.forms'];

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.Utilities', 'utilities', 'forms');
    }

    public function index_onDelete() {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $id) Form::findOrFail($id)->delete();
            Flash::success('Forms deleted.');
        }
        else {
            Flash::error('Couldn\'t find the IDs');
        }
        return $this->listRefresh();
    }

}
