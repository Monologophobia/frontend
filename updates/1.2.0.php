<?php namespace Monologophobia\Utilities\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class onePointTwoPointZero extends Migration
{
    public function up()
    {
        Schema::create('monologophobia_utilities_galleries', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('monologophobia_utilities_galleries');
    }
}