<?php namespace Monologophobia\Design;

use Schema;
use October\Rain\Database\Updates\Migration;

class onePointOnePointZero extends Migration {

    public function up() {

        Schema::create('mono_utilities_forms', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->json('fields');
            $table->string('notify')->nullable();
            $table->text('autoresponse')->nullable();
            $table->string('autoresponse_email')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('mono_utilities_form_records', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('form_id')->unsigned()->index();
            $table->foreign('form_id')->references('id')->on('mono_utilities_forms')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->json('data');
            $table->timestamps();
        });

    }

    public function down() {
        Schema::dropIfExists('mono_utilities_form_records');
        Schema::dropIfExists('mono_utilities_forms');
    }

}
