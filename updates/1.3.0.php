<?php namespace Monologophobia\Utilities\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class onePointThreePointZero extends Migration
{
    public function up()
    {
        if (Schema::hasTable('rainlab_blog_posts')) {
            Schema::table('rainlab_blog_posts', function($table) {
                $table->json('custom')->nullable();
            });
        }
        
    }
    
    public function down()
    {
        if (Schema::hasTable('rainlab_blog_posts')) {
            Schema::table('rainlab_blog_posts', function($table) {
                $table->dropColumn('custom');
            });
        }
    }
}