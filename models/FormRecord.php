<?php namespace Monologophobia\Utilities\Models;

use Log;
use Auth;
use Mail;
use Throwable;
use \October\Rain\Database\Model;

class FormRecord extends Model {

    // The table to use
    public $table = 'mono_utilities_form_records';
    public $timestamps = true;

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'form_id' => 'required|integer',
        'data' => 'required',
    ];

    use \October\Rain\Database\Traits\Encryptable;
    protected $encryptable = ['data'];

    protected $dates = ['created_at', 'updated_at'];
    protected $casts = ['data' => 'object'];

    public $belongsTo = [
        'form' => ['Monologophobia\Utilities\Models\Form', 'key' => 'form_id'],
        'user' => ['RainLab\User\Models\User', 'key' => 'user_id', 'scope' => 'withTrashed'],
    ];

    public function afterCreate() {
        try {

            $data = $this->generateData();
            $params = [
                "form_name" => $this->form->name,
                "autoresponse" => $this->form->autoresponse,
                "results" => $data->results,
                "orphaned" => $data->orphaned,
            ];

            if ($this->form->notify) {
                Mail::sendTo($this->form->notify, 'monologophobia.utilities::forms.notification', $params);
            }

        }
        catch (Throwable $e) {
            Log::error($e);
        }
    }

    public function generateData() {

        $results = [];
        // create an array to handle orphaned data
        $orphaned = [];

        if (count($this->data) > 0) {

            foreach ($this->data as $key => $data) {

                $found = false;

                foreach ($this->form->fields as $field) {
                    if ($field["slug"] == $key) {
                        $results[$field["name"]] = $data;
                        $found = true;
                        break;
                    }
                }

                // If we haven't found the related title for this dataset, add it to the orphaned pile
                if (!$found) $orphaned[] = $data;

            }
        
        }

        return (object) ["results" => $results, "orphaned" => $orphaned];

    }

}