<?php namespace Monologophobia\Utilities\Models;

use \October\Rain\Database\Model;

class Form extends Model {

    public $table = 'mono_utilities_forms';
    public $timestamps = true;

    use \October\Rain\Database\Traits\SoftDelete;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    use \October\Rain\Database\Traits\Nullable;
    protected $nullable = ['notify', 'autoresponse', 'autoresponse_email'];

    protected $jsonable = ['fields'];

    use \October\Rain\Database\Traits\Sluggable;
    protected $slugs = ['slug' => 'name'];

    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name' => 'required|string',
    ];

    public $hasMany = [
        'records' => ['Monologophobia\Utilities\Models\FormRecord', 'key' => 'form_id', 'delete' => true]
    ];

}