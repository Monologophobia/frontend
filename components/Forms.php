<?php namespace Monologophobia\Utilities\Components;

use Lang;
use Auth;
use Flash;
use Request;
use Redirect;
use Throwable;
use AjaxException;
use Cms\Classes\ComponentBase;
use Monologophobia\Utilities\Models\Form;
use Monologophobia\Utilities\Models\FormRecord;

class Forms extends ComponentBase {

    public function componentDetails() {
        return [
            'name'        => Lang::get('monologophobia.utilities::lang.components.forms.name'),
            'description' => Lang::get('monologophobia.utilities::lang.components.forms.description')
        ];
    }

    public function defineProperties() {
        return [
            'form' => [
                'title' => Lang::get('monologophobia.utilities::lang.components.forms.which'),
                'type'  => 'dropdown',
            ]
        ];
    }

    public function getFormOptions() {
        return Form::lists('name', 'id');
    }

    public $form;
    public $message;

    public function onRun() {

        try {

            $form_id = intval($this->property('form'));
            $this->form = Form::findOrFail($form_id);

            // Non-AJAX fallback
            if (post($this->form->name, false)) {
                $this->onFormSubmit();
                return Redirect::refresh();
            }

        }
        catch (Throwable $e) {
            $this->message = $e->getMessage();
        }

    }


    public function onFormSubmit() {
        try {

            $form_id = intval(post('form_id'));
            $form    = Form::findOrFail($form_id);
            $post    = post($form->slug);

            // check honeypot
            if (!empty(post('not-for-people', false))) {
                Flash::success(Lang::get('monologophobia.utilities::lang.components.forms.honeypot'));
                return;
            }

            // gather data
            foreach ($form->fields as $field) {
                $slug = $field["slug"];
                $type = $field["type"];
                if (isset($post[$slug])) {
                    $value = $this->sanitiseData($post[$slug], $type);
                    $data[$slug] = $value;
                }
            }

            $this->recordData($form, $data);

            $this->message = $form->autoresponse ? $form->autoresponse : Lang::get('monologophobia.utilities::lang.components.forms.success');

            return [
                '#form-message' => $this->renderPartial('@message')
            ];

        }
        catch (Throwable $e) {
            if (Request::ajax()) {
                throw new AjaxException($e);
            }
            throw $e;
        }
    }

    /**
     * Sanitise data based on type
     * @param Mixed value
     * @param String type
     * @return String
     */
    private function sanitiseData($value, $type) : string {
        if ($type == "number") {
            $value = filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT);
        }
        else if ($type == "email") {
            $value = filter_var($value, FILTER_SANITIZE_EMAIL);
        }
        else if ($type == "date") {
            $value = date('Y-m-d H:i:s', strtotime($value));
        }
        else if ($type == "url") {
            $value = filter_var($value, FILTER_SANITIZE_URL);
        }
        else {
            $value = filter_var($value, FILTER_SANITIZE_STRING);
        }
        return $value;
    }

    /**
     * Record the data to the database
     * @param Form
     * @param Array of data
     */
    private function recordData(Form $form, array $data) {
        $user   = (class_exists("\Auth")) ? \Auth::getUser() : false;
        $record = new FormRecord;
        $record->form_id = $form->id;
        $record->user_id = $user ? $user->id : null;
        $record->data    = $data;
        $record->save();
    }

}

?>
