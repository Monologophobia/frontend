<?php namespace Monologophobia\Utilities\Components;

use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Monologophobia\Utilities\Models\Gallery as GalleryModel;

class Gallery extends ComponentBase {

    public function componentDetails() {
        return [
            'name'        => 'Gallery',
            'description' => 'Displays a gallery as a grid or a slideshow'
        ];
    }

    public function defineProperties() {
        return [
            'gallery' => [
                'title' => 'Gallery to display',
                'type'  => 'dropdown',
            ],
            'grid_slideshow' => [
                'title'   => 'Display Type',
                'type'    => 'dropdown',
                'options' => ['grid' => 'Grid', 'slideshow' => 'Slideshow'],
                'default' => 'grid'
            ],
            "columns" => [
                "title"   => "Grid Columns",
                "type"    => "dropdown",
                "default" => "3",
                "options" => [1 => "1", 2 => "2", 3 => "3", 4 => "4", 5 => "5", 6 => "6"],
                "group"   => "Grid Options",
            ],
            "spacing" => [
                "title"   => "Grid Spacing",
                "type"    => "dropdown",
                "default" => "Normal",
                "options" => ["Normal" => "Normal", "Condensed" => "Condensed", "No Gap" => "No Gap"],
                "group"   => "Grid Options",
            ],
            "masonry" => [
                "title" => "Masonry Style",
                "type"  => "checkbox",
                "group" => "Grid Options",
            ],
            "autoplay" => [
                "title"   => "Autoplay",
                "type"    => "checkbox",
                "default" => true,
                "group"   => "Slideshow Options",
            ],
            "arrows" => [
                "title"   => "Navigation Arrows",
                "type"    => "checkbox",
                "default" => false,
                "group"   => "Slideshow Options",
            ],
            "dots" => [
                "title"   => "Dot Pagination",
                "type"    => "checkbox",
                "default" => true,
                "group"   => "Slideshow Options",
            ],
            "text_overlay" => [
                "title"   => "Overlay Image Text",
                "type"    => "checkbox",
                "default" => false,
                "group"   => "Slideshow Options",
            ],
            "constrained_height" => [
                "title"       => "Constrain Height",
                "description" => "Forces slideshow to display at a specific height. Any CSS unit can be used. Images will fit into the space and crop any excess. Leave blank for no constraints.",
                "type"        => "text",
                "group"       => "Slideshow Options",
            ],
            "slides" => [
                "title"       => "Slides",
                "description" => "Slides to display at once",
                "default"     => 1,
                "type"        => "text",
                "group"       => "Slideshow Options",
            ],
        ];
    }

    public function getGalleryOptions() {
        return GalleryModel::lists('name', 'id');
    }

    public $gallery;
    public $type;
    public $columns = 3;
    public $masonry = false;
    public $spacing = "Normal";
    public $autoplay = true;
    public $arrows   = false;
    public $dots     = true;
    public $slides   = 1;
    public $text_overlay = false;
    public $constrained_height = false;

    public function onRun() {

        $this->gallery = GalleryModel::findOrFail(intval($this->property('gallery')));
        $this->type    = $this->property('grid_slideshow');

        // grid settings
        $this->columns = intval($this->property('columns')) ?? 3;
        if (!$this->columns) $this->columns = 3;
        $this->spacing = $this->property('spacing') ?? "Normal";
        $this->masonry = boolval($this->property('masonry'));

        // slideshow settings
        $this->autoplay     = boolval($this->property('autoplay'));
        $this->arrows       = boolval($this->property('arrows'));
        $this->dots         = boolval($this->property('dots'));
        $this->slides       = intval($this->property('slides') ?? 1);
        $this->text_overlay = boolval($this->property('text_overlay'));
        $this->constrained_height = $this->property('constrained_height');

        if ($this->property('grid_slideshow') == 'grid') {
            $this->addCss('https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css');
            $this->addJs('https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js');
        }
        else {
            $this->addCss("https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css");
            $this->addCss("https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css");
            $this->addJs("https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js");
        }
    }

}
