<?php namespace Monologophobia\Utilities;

use App;
use Lang;
use Event;
use Cms\Classes\Theme;
use Backend\Facades\Backend;
use System\Classes\PluginBase;
use Cms\Classes\CmsController;
use System\Classes\PluginManager;
use Backend\FormWidgets\RichEditor;
use System\Classes\SettingsManager;

/**
 * CSRF For Frontend details
 * 
 * Automatically verifies AJAX POST requests
 * Non AJAX POST requires an additional step
 * 
 * All Layouts should have -
 * <meta name="csrf-token" content="{{ csrf_token() }}">
 * 
 * AJAX framework needs setting up -
 * <script>
 * $.ajaxSetup({
 *   headers: {
 *       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
 *   }
 * });
 * </script>
 * 
 * Non-AJAX POST requests should contain -
 * {{ form_token() }}
 * to generate correct data
 *
 */

class Plugin extends PluginBase {

    /**
     * Returns information about this plugin.
     * @return array can be null
     */
    public function pluginDetails() {
        return [
            'name'        => 'monologophobia.utilities::lang.plugin.name',
            'description' => 'monologophobia.utilities::lang.plugin.description',
            'author'      => 'Monologophobia',
            'icon'        => 'monologophobia.utilities::lang.plugin.icon',
        ];
    }

    /**
     * Register plugin permissions
     * @return array can be null
     */
    public function registerPermissions() {
        return [
            'monologophobia.utilities.variables' => ['tab' => Lang::get('monologophobia.utilities::lang.plugin.name'), 'label' => Lang::get('monologophobia.utilities::lang.permissions.variables')],
            'monologophobia.utilities.forms' => ['tab' => Lang::get('monologophobia.utilities::lang.plugin.name'), 'label' => Lang::get('monologophobia.utilities::lang.permissions.forms')],
        ];
    }

    /**
     * Register plugin settings
     * @return array can be null
     */
    public function registerSettings() {
        return [
            'csrf_settings' => [
                'label' => Lang::get('monologophobia.utilities::lang.csrf.name'),
                'description' => Lang::get('monologophobia.utilities::lang.csrf.description'),
                'icon' => 'icon-link',
                'class' => 'Monologophobia\Utilities\Models\CSRFSettings',
                'category' => SettingsManager::CATEGORY_CMS,
                'permissions' => ['monologophobia.utilities.variables'],
            ],
        ];
    }

    // Register front-end components
    public function registerComponents() {
        return [
           '\Monologophobia\Utilities\Components\Forms'   => 'forms',
           '\Monologophobia\Utilities\Components\Gallery' => 'gallery',
        ];
    }

    // Register components for use with static pages
    public function registerPageSnippets() {
        return [
            '\Monologophobia\Utilities\Components\Forms'   => 'forms',
            '\Monologophobia\Utilities\Components\Gallery' => 'gallery',
        ];
    }

    /**
     * Add twig tags. Specifically CSRF Token
     */
    public function registerMarkupTags() {
        return [
            'functions' => [
                'csrf_token' => static function () {
                    return csrf_token();
                },
            ],
        ];
    }

    // Create the backend navigation
    public function registerNavigation() {
        return [
            'forms' => [
                'label'       => Lang::get('monologophobia.utilities::lang.forms.name'),
                'url'         => Backend::url('monologophobia/utilities/forms'),
                'icon'        => Lang::get('monologophobia.utilities::lang.forms.icon'),
                'permissions' => ['monologophobia.utilities.forms'],
                'order'       => 599,
            ]
        ];
    }

    public function registerReportWidgets() {

    }

    public function registerMailTemplates() {
        return [
            'monologophobia.utilities::forms.notification' => Lang::get('monologophobia.utilities::lang.components.forms.notification'),
            'monologophobia.utilities::forms.autoresponse' => Lang::get('monologophobia.utilities::lang.components.forms.autoresponse'),
        ];
    }

    public function boot() {

        // Add jsonable to Post model if exists
        if (class_exists('\RainLab\Blog\Models\Post')) {
            \RainLab\Blog\Models\Post::extend(function($model) {
                $model->addJsonable(['custom']);
            });
        }

        // Is this being called from the frontend?
        if (!App::runningInBackend()) {
            // if so, do the CSRF stuff
            CmsController::extend(static function (CmsController $controller) {
                $controller->middleware(VerifyCsrfTokenMiddleware::class);
            });
            // and nothing else
            return;
        }

        RichEditor::extend(function($widget) {
            $widget->addJs('/plugins/monologophobia/utilities/assets/js/froala.plugins.js');
            $theme = Theme::getActiveTheme();
            $widget->addCss("/themes/{$theme->getDirName()}/assets/css/grid.css");
        });

        // extend Media manager to include a side menu for the galleries system
        Event::listen('backend.menu.extendItems', function($manager) {
            $manager->addSideMenuItems('October.Backend', 'media', [
                'media' => [
                    'label'       => Lang::get('backend::lang.media.menu_label'),
                    'url'         => Backend::url('backend/media'),
                    'icon'        => 'icon-file',
                    'permissions' => ['media.*'],
                    'order'       => 1,
                ],
                'galleries' => [
                    'label'       => 'Galleries',
                    'url'         => Backend::url('monologophobia/utilities/galleries/'),
                    'icon'        => 'icon-file-image-o',
                    'permissions' => ['media.*'],
                    'order'       => 2,
                ],
            ]);
        });

        Event::listen('backend.page.beforeDisplay', function($controller, $action, $params) {
            if ($controller instanceof \RainLab\Blog\Controllers\Posts) {
                $controller->addJs('/plugins/monologophobia/utilities/assets/js/blog_tools.js');
            }
            if ($controller instanceof \RainLab\Pages\Controllers\Index) {
                $controller->addJs('/plugins/monologophobia/utilities/assets/js/page_tools.js');
            }
        });

        // replace the blog editor with froala
        Event::listen('backend.form.extendFields', function ($form) {
            if ($form->model instanceof \RainLab\Blog\Models\Post) {
                foreach ($form->getFields() as $field) {
                    if (!empty($field->config['type']) && $field->config['type'] == "RainLab\Blog\FormWidgets\BlogMarkdown") {
                        $field->config['type'] = $field->config['widget'] = 'richeditor';
                        return;
                    }
                }
                if ($form->isNested === false) {
                    $form->addSecondaryTabFields([
                        'custom' => [
                            'label' => 'Custom Fields',
                            'tab'   => 'Custom',
                            'type'  => 'repeater',
                            "form"  => [
                                "fields" => [
                                    "label" => [
                                        "label" => "Label",
                                        "span"  => "auto",
                                        "type"  => "text",
                                    ],
                                    "value" => [
                                        "label" => "Value",
                                        "span"  => "auto",
                                        "type"  => "text",
                                    ]
                                ]
                            ]
                        ],
                    ]);
                }
            }
        });

    }

    public function registerSchedule($schedule) {
        /*$schedule->call(function() {
            
        })->monthly();*/
    }

}
